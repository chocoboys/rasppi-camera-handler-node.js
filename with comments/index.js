var express = require('express');
var app = express()
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

// Подключение модуля для создания дочернего процесса
const spawn = require('child_process').spawn;

const fs = require("fs");

server.listen(8008);

// Папка для размещения статических файлов
app.use(express.static(__dirname + "/static"));

app.get('/', function(request, response) {
	response.sendFile(__dirname + '/index.html');
});

io.sockets.on('connection', function(socket) {
	console.log('Установлено соединение с клиентом');
	
	socket.on('disconnect', function() {
		console.log('Соединение с клиентом прервано');
	});
	
	
	socket.on('request_data', function() {
		
		// Запуск дочернего процесса для создания изображения
		const child = spawn('./script.sh', []);
		
		var arrays = [];
		
		child.stdout.on('data', (data) => {
			arrays.push(data);

			//fs.writeFileSync('photo', data, function() {});
		});
		
		child.stderr.on('data', (data) => {
			console.log(`stderr: ${data}`);
		});

		// После завершения процесса
		child.on('close', () => {
				
			// Длина общего массива данных
			

			//console.log(typeof(data_array));
			//console.log(data_array[data_array.length - 1]);
			//for (prop in data_array)
			//	console.log(prop);

			io.sockets.emit('send_data', arrays);	

			// fs.readFile('photo', (err, data) => {
			// 	if(err) throw err;

			// 	// Отправка клиенту
			// 	io.sockets.emit('send_data', data);		
			// });
			//Удаление файла
			//fs.unlink('photo', function() {});
		});
	
	});
});
