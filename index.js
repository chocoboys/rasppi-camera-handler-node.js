var express = require('express');
var app = express()
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var cv = require('opencv4nodejs');
var Splitter = require("stream-split");
var fs = require('fs');
const spawn = require('child_process').spawn;
const {performance} = require('perf_hooks');

// Разделитель кадров
const NALseparator = new Buffer([0, 0, 0, 1]);
// Число пальцев руки в объективе
var finger_count = 0;

server.listen(8008);

// Папка для размещения статических файлов
app.use(express.static(__dirname + "/static"));

app.get('/', function(request, response) {
	response.sendFile(__dirname + '/index.html');
});

// Функция обработки изображения библиотекой opencv
function process_image() {
	// Дииапазон тонов кожи, формат HSV
	var skinColorLower = new cv.Vec(150,  0, 0);
	var skinColorUpper = new cv.Vec(170, 100, 225);
	// Максимальное количество выделяемых точек
	const maxPointDist = 25;
	// Нахождение расстояния между двумя точками
	const ptDist = (pt1, pt2) => pt1.sub(pt2).norm();
	// Получение центра точки
	const getCenterPt = pts => pts.reduce((sum, pt) => sum.add(pt), new cv.Point(0, 0)).div(pts.length);
	// Максимальный размер угла beta
	const maxAngleDeg = 60;

	// При появлении файла в директории
	fs.access('./out.jpg', fs.F_OK, function(err) {
		if(!err)
		{
			// Загрузка изображения
			var img = cv.imread('./out.jpg');

			// Выделяем контур руки
			var imgHLS = img.cvtColor(cv.COLOR_BGR2HLS);
			var handMask = imgHLS.inRange(skinColorLower, skinColorUpper);

			// Удаление шумов с объекта
			handMask = handMask.blur(new cv.Size(10, 10));
			handMask = handMask.threshold(200, 255, cv.THRESH_BINARY);

			// Выделяем контуры
			var contours = handMask.findContours(cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE);
			// Самый большой из них, вероятно, он и будет контуром руки
			var handContour = contours.sort((c0, c1) => c1.area - c0.area)[0];

			// Присутствует ли вообще рука в кадре
			if (typeof(handContour) !== 'undefined')
			{
				// *** 
				// Выделяем контур только с одной точкой в каждой области выделения
				// ***
				var hullIndices = handContour.convexHullIndices();
				// Точки контура
				var contourPoints = handContour.getPoints();
				var hullPointsWithIdx = hullIndices.map(idx => ({
					pt: contourPoints[idx],
					contourIdx: idx
				}));
				var hullPoints = hullPointsWithIdx.map(ptWithIdx => ptWithIdx.pt);

				// Группируем все точки, найденные в одной локальной окрестности
				var ptsBelongToSameCluster = (pt1, pt2) => ptDist(pt1, pt2) < maxPointDist;
				var { labels } = cv.partition(hullPoints, ptsBelongToSameCluster);
				var pointsByLabel = new Map();
				labels.forEach(l => pointsByLabel.set(l, []));
				hullPointsWithIdx.forEach((ptWithIdx, i) => {
					var label = labels[i];
					pointsByLabel.get(label).push(ptWithIdx);
				});

				// Функция получения центральной точки из всего набора точек
				const getMostCentralPoint = (pointGroup) => {
					var center = getCenterPt(pointGroup.map(ptWithIdx => ptWithIdx.pt));
					return pointGroup.sort(
					(ptWithIdx1, ptWithIdx2) => ptDist(ptWithIdx1.pt, center) - ptDist(ptWithIdx2.pt, center)
					)[0];
				};

				var pointGroups = Array.from(pointsByLabel.values());

				hullIndices = pointGroups.map(getMostCentralPoint).map(ptWithIdx => ptWithIdx.contourIdx);

				// *** 
				// Находим координаты кончиков ппалььцев
				// ***
				var defects = handContour.convexityDefects(hullIndices);
				var handContourPoints = handContour.getPoints();
				var hullPointDefectNeighbors = new Map(hullIndices.map(idx => [idx, []]));
				defects.forEach((defect) => {
					const startPointIdx = defect.at(0);
					const endPointIdx = defect.at(1);
					const defectPointIdx = defect.at(2);
					hullPointDefectNeighbors.get(startPointIdx).push(defectPointIdx);
					hullPointDefectNeighbors.get(endPointIdx).push(defectPointIdx);
				});

				var vertices = Array.from(hullPointDefectNeighbors.keys())
					.filter(hullIndex => hullPointDefectNeighbors.get(hullIndex).length > 1)
					.map((hullIndex) => {
					const defectNeighborsIdx = hullPointDefectNeighbors.get(hullIndex);
					return ({
						pt: handContourPoints[hullIndex],
						d1: handContourPoints[defectNeighborsIdx[0]],
						d2: handContourPoints[defectNeighborsIdx[1]]
					});
				});

				verticesWithValidAngle = vertices.filter((v) => {
					const sq = x => x * x;
					const a = v.d1.sub(v.d2).norm();
					const b = v.pt.sub(v.d1).norm();
					const c = v.pt.sub(v.d2).norm();
					const angleDeg = Math.acos(((sq(b) + sq(c)) - sq(a)) / (2 * b * c)) * (180 / Math.PI);
					return angleDeg < maxAngleDeg;
				});

				
				img.drawContours([handContour.getPoints()], 0, new cv.Vec(255, 0, 0), {thickness: 2});
				
				// Узнаем число пальцев, показываемых в кадре
				finger_count = verticesWithValidAngle.length;
				
				verticesWithValidAngle.forEach((v) => {
					img.drawLine(
					v.pt,
					v.d1,
					{ color: new cv.Vec(0, 255, 0), thickness: 2 }
					);
					img.drawLine(
					v.pt,
					v.d2,
					{ color: new cv.Vec(0, 255, 0), thickness: 2 }
					);
					img.drawEllipse(
					new cv.RotatedRect(v.pt, new cv.Size(20, 20), 0),
					{ color: new cv.Vec3(255, 0, 0), thickness: 2 }
					);
				});

				// Сохраняем результат
				cv.imwrite('./new.jpg', img);
				// Отправляем результат клиенту
				fs.readFile('./new.jpg', "binary", (err, data) => {
					io.sockets.emit('send_image', { "img": data, "count": finger_count});
				});
			}
		}
	});
};

io.sockets.on('connection', function(socket) {
	console.log('Установлено соединение с клиентом');
	
	socket.on('disconnect', function() {
		console.log('Соединение с клиентом прервано');
	});
	
	
	socket.on('request_data', function(){
		const child = spawn('ffmpeg', [
            '-f', 'v4l2',
			'-i', '/dev/video0',
			// Первый поток
            '-s', 640 + 'x' + 360,
            '-r', 30,
            '-pix_fmt', 'yuv420p',
            '-c:v', 'libx264',
            '-b:v', '600k',
            '-bufsize', '600k',
            '-vprofile', 'baseline',
            '-tune', 'zerolatency',
			'-f', 'rawvideo',
			'-y',
			'-',
			// Второй поток
			'-s', 250 + 'x' + 250,
            '-r', 1,
			'-pix_fmt', 'bgr8',
			'-update', '1',
            'out.jpg'
		]);

		// Обновляем изображение каждую секунду
		setInterval(process_image, 1000);

		child.on("exit", function (code) {
            console.log("Failure", code);
        });
		

		child.stdout.pipe(new Splitter(NALseparator)).on('data', (dataunit) => {
			io.sockets.emit('send_data', Buffer.concat([NALseparator, dataunit]));
		});

	});

});

